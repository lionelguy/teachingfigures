################################################################################
# Some plots about distribution of genome size and n proteins
################################################################################
# Retrieve data 
for GROUP in eukaryotes prokaryotes viruses; do
    wget ftp://ftp.ncbi.nlm.nih.gov/genomes/GENOME_REPORTS/$GROUP.txt
done

Rscript doStuff.R
