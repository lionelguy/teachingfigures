# Get a GC vs size plot
#mv ~/Downloads/genomes_proks.txt .
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/GENOME_REPORTS/prokaryotes.txt
grep "Complete Genome" prokaryotes.txt | cut -f 1-9,16-20 > genomes_proks_reduced.tab
## R

df <- read.table("genomes_proks_reduced.tab", h=F, sep="\t", quote=NULL, comment.char="", stringsAsFactors=F)
names(df) <- c("name", "taxid", "bioproject_acc", "bioproject_id", "group", "subgroup", "size_mb", "GC", "genes", "proteins", "release", "modification", "status")
df$release <- as.Date(df$release)
df$modification <- as.Date(df$modification)
df$GC <- as.numeric(df$GC)
df$size_mb <- as.numeric(df$size_mb)
df$proteins <- as.numeric(df$proteins)

plot(GC ~ size_mb, data=df, log="x", cex=0.4, ylim=c(10,75))

## ggplot2
library(ggplot2)
dates <- c("2006-01-01", "2014-01-01", "2017-09-01")

for (date in dates){
  jpeg(paste("GC_vs_size_", date, ".jpg", sep= ""), h=300, w=400, qual=100)
  p1 <- ggplot(subset(df, release < date), aes(size_mb, GC))
  print(p1 + geom_point(alpha=0.5) + 
    scale_x_log10("Size (Mb)", breaks=c(0.1,0.2,0.5,1,2,5,10), 
      limits=c(0.1, 15)) + 
        scale_y_continuous("GC content", breaks = seq(10, 80, by=10), 
          limits=c(10,80)))
  dev.off()
}
